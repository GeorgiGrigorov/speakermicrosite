<?php
ini_set('display_errors',1);
ini_set('log_errors', 1);
ini_set('error_log', dirname(__FILE__) . '/SpeakerMicroSite_error_log.txt');
error_reporting(E_ALL^E_NOTICE);

// Names and stuff
define('URL',get_bloginfo('url'));
define('HOME',$home_url);
define('HOME_URL', home_url());
define('NAME',get_bloginfo('name'));
define('DESCRIPTION',get_bloginfo('description'));


// Define folder constants
define('ROOT', get_bloginfo('template_url'));


define('JS', ROOT . '/js');
define('IMG', ROOT . '/img');
define('CSS', ROOT . '/css');
define('SRC', ROOT . '/source');

add_theme_support( 'menus' );

add_action( 'after_setup_theme', 'speakermicrosite_setup' );
add_action( 'wp_enqueue_scripts', 'speakermicrosite_scripts' );



function speakermicrosite_setup() {
    register_nav_menu('main-menu', __( 'Main Menu', 'speakermicrosite' ));
    register_nav_menu('footer-menu', __( 'Footer Menu', 'speakermicrosite' ));

    get_role('editor')->add_cap('edit_theme_options');

}


add_action( 'init', 'SpeakerMicroSite_post_types', 0 );
function SpeakerMicroSite_post_types() {

    $args_quotes = array(
        'labels'             => array( 'name' => __('Quotes', 'speakermicrosite'), 'singular_name' =>  __('Quote', 'speakermicrosite'), ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'quote', 'with_front' => true,  ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'excerpt', 'revisions', 'custom-fields'  )
    );

    $args_tip = array(
        'labels'             => array( 'name' => __('Tips', 'speakermicrosite'), 'singular_name' =>  __('Tip', 'speakermicrosite'), ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'tip', 'with_front' => true,  ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'excerpt', 'revisions', 'custom-fields'  )
    );

    $args_videos = array(
        'labels'             => array( 'name' => __('Videos', 'speakermicrosite'), 'singular_name' =>  __('Video', 'speakermicrosite'), ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'video', 'with_front' => true,  ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'excerpt', 'revisions', 'custom-fields'  )
    );
    $args_slider = array(
        'labels'             => array( 'name' => __('Slider for Homepage', 'speakermicrosite'), 'singular_name' =>  __('Slider Home', 'speakermicrosite'), ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'slider_home', 'with_front' => true,  ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'excerpt', 'revisions', 'custom-fields'  )
    );

    $args_books = array(
        'labels'             => array( 'name' => __('Books', 'speakermicrosite'), 'singular_name' =>  __('Book', 'speakermicrosite'), ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'book', 'with_front' => true,  ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'excerpt', 'revisions', 'custom-fields'  )
    );
    $args_firms = array(
        'labels'             => array( 'name' => __('Firms', 'speakermicrosite'), 'singular_name' =>  __('Firm', 'speakermicrosite'), ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'firm', 'with_front' => true,  ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'excerpt', 'revisions', 'custom-fields'  )
    );
    $args_topics = array(
        'labels'             => array( 'name' => __('Key Topics', 'speakermicrosite'), 'singular_name' =>  __('Key topic', 'speakermicrosite'), ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'topic', 'with_front' => true,  ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'excerpt', 'revisions', 'custom-fields'  )
    );

    register_post_type( 'slider_home',  $args_slider );
    register_post_type( 'quote',  $args_quotes );
    register_post_type( 'topic',  $args_topics );
    register_post_type( 'firm',  $args_firms );
    register_post_type( 'tip', 		$args_tip );
    register_post_type( 'book', 	$args_books );
    register_post_type( 'video', 	$args_videos );

    global $halftime_taxonomies;
    foreach($halftime_taxonomies as $key => $value){
        register_taxonomy( $key , $value['post_type'], array(
            'hierarchical' 		=> true,
            'label' 			=> $value['label'],
            'query_var' 		=> true,
            'rewrite' 			=> true,
            'show_ui' 			=> true,
            'show_admin_column' => false,
        ));
    }
}




function speakermicrosite_scripts(){

    if( !is_admin()) {

        wp_deregister_script('jquery');
        wp_enqueue_script('jquery', JS . '/jquery-3.3.1.min73aa35eb3131d72d6a90493904bfa116c73a6799060e1fdc7ce5da8aedb8e066.js', array(), "1.0", false);
        wp_enqueue_script('bootstrap-js', JS . '/bootstrap.min.js', array('jquery'), "1.0", false);
        wp_enqueue_script('fancybox-js', JS . '/jquery.fancybox.min.js', array('jquery'), "1.0", false);
        wp_enqueue_script('matchheight-js', JS . '/jquery.matchHeight.js', array('jquery'), "1.0", false);
        wp_enqueue_script('wow-js', JS . '/wow.min.js', array('jquery'), "1.0", false);
        wp_deregister_script('main-js');
        wp_enqueue_script('moment-js', JS . '/moment.min5a05ad5280c4303493f4f209e1ecfa32c9750b600d755281fbf40d0f47059e89.js', array('jquery'), "1.1", false);
        wp_enqueue_script('moment-timezone-js', JS . '/moment-timezone-with-data-2012-2022.mine24243eecebfb484d17a2a66258da0a83fe00243b2c1ba04ff15852ff6454309.js', array('jquery'), "1.1", false);
        wp_enqueue_script('countdown-js', JS . '/jquery.countdown.min.js', array('jquery'), "1.1", false);
        wp_enqueue_script('main-js', JS . '/main.js', array('jquery'), "1.1", false);
        wp_enqueue_style('bootstrap-css', CSS . '/bootstrap.min.css');
        wp_enqueue_style('fancybox-css', CSS . '/jquery.fancybox.min.css');
        wp_enqueue_style('google-font-css', 'https://fonts.googleapis.com/css?family=Roboto:100,400,700,900&amp;subset=greek');
        wp_enqueue_style('mainstyle-css', ROOT . '/style.css', "1.2");



    }
}

